// Your code goes here
let word;
let isValid = true
while(isValid) {
    word = prompt('Please, input a word');
    if(word === '' || word.trim() === '') {
        alert('Invalid value');
    } else {
        var mid;
        if(word.length % 2 === 0) {
            if(word[word.length / 2 - 1] === word[word.length / 2]) {
                alert('Middle characters are the same');
            } else {
                mid = word[word.length / 2 - 1] + word[word.length / 2];
                alert(mid);
            }
        }else if(word.length % 2 !== 0) {
            mid = word[Math.floor(word.length / 2)];
            alert(mid);
        }else if(word.length === 1) {
            alert(word);
        }
        isValid = false
    }
}