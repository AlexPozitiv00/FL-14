// Your code goes here
let amoutOfBatteries;
let percent;
let defective;
let working;
let isValid = true;

while(isValid) {
    amoutOfBatteries = +prompt('Amount of Batteries');
    percent = +prompt('Defective rate');
    if(typeof amoutOfBatteries === 'number' && amoutOfBatteries >= 0 &&
        typeof percent === 'number' && percent >= 0 && percent <= 100) {
            percent /= 100;
            defective = amoutOfBatteries * percent;
            working = amoutOfBatteries - defective;
            isValid = false;
    } else {
        alert('Invalid input data');
    }
}
alert(`Amount of batteries: ${amoutOfBatteries}
Defective rate: ${percent * 100}%
Amount of defective batteries: ${defective.toFixed(2)}
Amount of working batteries:  ${working.toFixed(2)}
    `);