function visitLink(path) {
	//your code goes here
	let counter = localStorage.getItem(path);
	if(counter === null) {
		counter = 0;
	}
		counter++;
		localStorage.setItem(path, counter);
}

function viewResults() {
	//your code goes here
	let block = document.querySelector('.mx-auto');
	if(block.lastChild.className !== 'paragraphs') {
		let div = document.createElement('div');
		div.classList.add('paragraphs');
		document.querySelector('.mx-auto').appendChild(div);

		if(localStorage.length === 0) {
			let p = document.createElement('p');
			p.innerHTML = 'No page was visited!';
			document.querySelector('.paragraphs').appendChild(p);
		} else {
			let i = 0;
			while(localStorage.length !== 0) {
				let p = document.createElement('p');
				p.innerHTML = `You visited ${localStorage.key(i)} ${localStorage.getItem(localStorage.key(i))} time(s)`;
				document.querySelector('.paragraphs').appendChild(p);
				localStorage.removeItem(localStorage.key(i));
			}
		}
	} 
	
}
