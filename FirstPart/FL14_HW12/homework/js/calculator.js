//your code goes here

let expr, res;
let boolex = true;
while (boolex) {
  expr = prompt("Введите выражение?");
  if (expr === null) {
    break;
  }
  try {
    res = eval(expr);
    if (isNaN(res)) {
      throw new Error("Результат неопределён");
    }

    break;
  } catch (e) {
    alert( "Ошибка: " + e.message + ", повторите ввод" );
  }
}

alert(res);