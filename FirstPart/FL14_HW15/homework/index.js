/* START TASK 1: Your code goes here */
let table = document.querySelector('.table');
let cells = document.getElementsByTagName('td');
table.addEventListener('click', function(e) {
    if(e.target && e.target.nodeName === 'TD' && findSelected(e.target.classList, 'first')) {
        let row = e.target.parentNode.children;
        for(let i = 0; i < row.length; i++) {
            if(!findSelected(row[i].classList, 'selected')) {
                row[i].style = 'background: blue';
                row[i].classList.add('selected');
            }
        }
    } else if(e.target.id === 'special') {
        table.style = 'background: yellow';
        
        for(let i = 0; i<cells.length; i++) {
            if(findSelected(cells[i].classList, 'selected') === false) {
                cells[i].style = 'background: yellow';
                cells[i].classList.add('selected');
            }
        }
    } else {
        e.target.style = 'background: yellow';
        e.target.classList.add('selected');
    }
})

function findSelected(elem, attribute) {
    let result = elem.contains(`${attribute}`);
    return result;
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
let textArea = document.querySelector('.text');
let phoneArea = document.getElementById('phone');
let submit = document.getElementById('sender');
submit.disabled = true;

phoneArea.onblur = function() {
    let reg = /^\+380(\d{9})$/;
    let result = reg.test(phoneArea.value);
    if(!result) {
        textArea.innerHTML = 'Type number doesn`t follow format +380*********';
        textArea.style = 'color: white; background: #ff8080';
    } else {
        submit.disabled = false;
        textArea.innerHTML = 'Success!';
        textArea.style = 'color: white; background: green';
    }
}
submit.onclick = function() {
    textArea.innerHTML = 'Data was successfully sent';
    phoneArea.value = '';
}
/* END TASK 2 */

/* START TASK 3: Your code goes here */

/* END TASK 3 */
