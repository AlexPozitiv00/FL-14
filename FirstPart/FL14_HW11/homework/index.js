// Your code goes here
function isEquals(a, b) {
    while(a > b) {
        return true;
    }
    return false;
}

function numberToString(num) {
    return num.toString();
}

function storeNames() {
    let arr = [];
    for(let i = 0; i < arguments.length; i++) {
        arr[i] = arguments[i];
    }
    return arr;
}

function getDivision(a, b) {
    if(isEquals(a, b)) {
        return a/b;
    } else {
        return b/a;
    }
}

function negativeCount(arr) {
    let count = 0;
    for(let i = 0; i < arr.length; i++) {
        if(arr[i] < 0) {
            count++;
        }
    }
    return count;
}

function letterCount(str1, str2) {
    let count = 0;
    for(let i = 0; i < str1.length; i++) {
        if(str1[i] === str2) {
            count++;
        }
    }
    return count;
}

function countPoints(matches) {
    let total = 0;
    for(let i = 0; i < matches.length; i++) {
        let mid = matches[i].indexOf(`:`);
        let a = +matches[i].substring(0, mid);
        let b = +matches[i].substr(mid+1);
        if(isEquals(a, b)) {
            total += 3;
        } else if(a === b) {
            total += 1;
        }
    }
    return total;
}
console.log('-----Task 1-----');
console.log(isEquals(3, 3));
console.log('-----Task 2-----');
console.log(numberToString(1234));
console.log('-----Task 3-----');
console.log(storeNames('Tomas Shelby', 'Ton Don', 'Eric Pascal'));
console.log('-----Task 4-----');
console.log(getDivision(2, 8));
console.log(getDivision(8, 4));
console.log('-----Task 5-----');
console.log(negativeCount([4, 3, 2, 9]));
console.log(negativeCount([4, -3, 2, 9]));
console.log('-----Task 6-----');
console.log(letterCount('Marry', 'r'));
console.log(letterCount('', 'z'));
console.log('-----Task 7-----');
console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']));