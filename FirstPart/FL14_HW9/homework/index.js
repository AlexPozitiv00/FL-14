// Your code goes here
// Your code goes here
function convert() {
    let arr = [];
    for(let i = 0; i < arguments.length; i++) {
        if(typeof arguments[i] === 'string') {
            arr[i] = Number(arguments[i]);
        } else {
            arr[i] = String(arguments[i]);
        }
    }
    return arr;
}

function executeforEach(arr, callback) {
    for(let i = 0; i < arr.length; i++) {
        arr[i] = callback(arr[i]);
    }
}

function mapArray(arr, callback) {
    let result = [];

    executeforEach(arr, function (el) {
        result.push(callback(parseInt(el)));
    })

    return result;
}

function filterArray(arr, callback) {
    let array = [];
    executeforEach(arr, (el) => {
		if (callback(el)){
			array.push(el);
		}
	});
    return array;
}

function containsValue(arr, arg) {
    for(let i = 0; i < arr.length; i++) {
        if(arr[i] === arg) {
            return i;
        }
    }
    return false;
}

function flipOver(str) {
    let str2 = '';
    for(let i = str.length - 1; i >= 0; i--) {
        str2 += str[i];
    }
    return str2;
}

function makeListFromRange(arr) {
    let start = arr[0];
    let end = arr[1];
    let num = start;
    let arr2 = [];
    for(let i = 0; i < end - start + 1; i++) {
        arr2[i] = num;
        num++;
    }
    return arr2;
}

function getArrayOfKeys(obj, keyName) {
    const result = [];

    executeforEach(obj, function (el) {
        result.push(el[keyName]);
    })

    return result;
}

function getTotalWeight(obj) {
    let result = 0;

    executeforEach(obj, function (el) {
        result += el['weight'];
    })

    return result;
}

function getPastDay(date, pastDays) {
  const mSecDay = 86400000;
  const pastDayInMsc = pastDays * mSecDay;
  const past = new Date(date - pastDayInMsc);
  return past.getDate();
}

function formatDate(date) {
    return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()} ${date.getHours()}:${date.getMinutes()}`;
}



console.log(convert('2', 4,'3'));
console.log(executeforEach([1,2,3], function(el) {
    console.log(el * 2)
} ));
console.log(mapArray([2, '5', 8], function(el) {
    return el + 3
} ));
console.log(filterArray([2, 5, 8], function(el) {
    return el % 2 === 0 
} ));
console.log(containsValue([2, 5, 8], 2));
console.log(flipOver('hey world'));
console.log(makeListFromRange([2, 7]));
const fruits = [
    { name: 'apple', weight: 0.5 },
    { name: 'pineapple', weight: 2 }
  ];
console.log(getArrayOfKeys(fruits, 'name'));

const basket = [
    { name: 'Bread', weight: 0.3 },
    { name: 'Coca-Cola', weight: 0.5 },
    { name: 'Watermelon', weight: 8 }
  ];
console.log(getTotalWeight(basket));
  
const date = new Date(2020, 0, 2);
console.log(getPastDay(date, 2));
console.log(getPastDay(date, 365));
console.log(formatDate(new Date('6/15/2018 09:15:00')));