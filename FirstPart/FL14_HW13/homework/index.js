// Your code goes here
let days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
function getAge(birthDate) {
    birthDate = new Date(birthDate);
    let now = new Date(),
    age = now.getFullYear() - birthDate.getFullYear();
    return now.setFullYear(1972) < birthDate.setFullYear(1972) ? age - 1 : age;
    
}

function getWeekDay(date) {
    date = new Date(date);
    let day = days[date.getDay()];
    return day;
}

function getProgrammersDay(year) {
    let progr = 256;
    let months = ['Январь', 'Февраль', 'Март', 'Апрель', 
                'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 
                'Октябрь', 'Ноябрь', 'Декабрь'];
    let date = new Date(year, 0, progr);
    let result = `${date.getDate()} ${months[date.getMonth()]}, ${year} (${getWeekDay(date)})`;
    return result;
}

function howFarIs(day) {
    let nextDay;
    let result
    let today = new Date().getDay();
    for(let i = 0; i < days.length; i++) {
        if(days[i].toLowerCase() === day.toLowerCase()) {
            nextDay = i;
            break;
        }
    }
    if(nextDay > today) {
        result = nextDay - today;
        return `It's ${result} day(s) left till ${day}`;
    } else if(nextDay < today) {
        let count = 0;
        let i = today;
        while(i < 7 && i > nextDay) {
            count++;
            if(i === 6) {
                i = 0;
            } else {
                i++;
            }
        }
        return `It's ${count} day(s) left till ${day}`;
    } else {
        return `Hey! today is ${day} =)`;
    }
}

function isValidIdentifier(word) {
    let result = /^[^0-9\s][0-9a-zA-z_$]*$/g.test(word);
    //let result2 = /!/.test(word);
    if(result) {
        return true;
    } else {
        return false;
    }
}

function capitalize(str) {
    return str.replace(/(^|\s)\S/g, function(a) {
        return a.toUpperCase()
    })
}

function isValidAudioFile(str) {
    let result = /^[a-z]+\.(mp3|flac|alac|aac)$/.test(str);
    if(result) {
        return true;
    } else {
        return false;
    }
}

function getHexadecimalColors(str) {
    let regExp = /#([a-f0-9]{3}){1,2}\b/gi;
    let result = str.match(regExp);
    if(result === null) {
        return [];
    } else {
        return result; 
    }
}

function isValidPassword(password) {
    let result = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(password);
    if(result) {
        return true;
    } else {
        return false;
    }
}

function addThousandsSeparators(str) {
    let regExp = /\B(?=(\d{3})+(?!\d))/g;
    let str2 = String(str).replace(regExp, ',');
    return str2;
}

console.log('---TASK 1---');
const birthday22 = new Date(2000, 9, 22);
const birthday23 = new Date(2000, 9, 30);
console.log(getAge(birthday22)); // 20 (assuming today is the 22nd October)
console.log(getAge(birthday23)); // 19 (assuming today is the 22nd October)

console.log('---TASK 2---');
console.log(getWeekDay(Date.now())); // "Thursday" (if today is the 22nd October)
console.log(getWeekDay(new Date(2020, 9, 22))); // "Thursday"

console.log('---TASK 3---');
console.log(getProgrammersDay(2020)); // "12 Sep, 2020 (Saturday)"
console.log(getProgrammersDay(2019)); // "13 Sep, 2019 (Friday)"

console.log('---TASK 4---');
console.log(howFarIs('воскресенье')); // "It's 1 day(s) left till Friday." (on October 22nd)
console.log(howFarIs('Четверг')); // "Hey, today is Thursday =)" (on October 22nd)

console.log('---TASK 5---');
console.log(isValidIdentifier('myVar!')); // false
console.log(isValidIdentifier('myVa_r$')); // true
console.log(isValidIdentifier('myVar_1')); // true
console.log(isValidIdentifier('1_myV$ar')); // false

console.log('---TASK 6---');
const testStr = 'My name is John Smith. I am 27.';
console.log(capitalize(testStr)); // "My Name Is John Smith. I Am 27."

console.log('---TASK 7---');
console.log(isValidAudioFile('file.mp4')); // false
console.log(isValidAudioFile('my_file.mp3')); // false
console.log(isValidAudioFile('file.mp3')); // true

console.log('---TASK 8---');
const testString = 'color: #3f3; background-color: #AA00ef; and: #abcd';
console.log(getHexadecimalColors(testString)); // ["#3f3", "#AA00ef"]
console.log(getHexadecimalColors('red and #0000')); // [];

console.log('---TASK 9---');
console.log(isValidPassword('agent007')); // false (no uppercase letter)
console.log(isValidPassword('AGENT007')); // false (no lowercase letter)
console.log(isValidPassword('AgentOOO')); // false (no numbers)
console.log(isValidPassword('Age_007')); // false (too short)
console.log(isValidPassword('Agent007')); // true


console.log('---TASK 10---');
console.log(addThousandsSeparators('1234567890')); // "1,234,567,890"
console.log(addThousandsSeparators(1234567890)); // "1,234,567,890"
