const quests = JSON.parse(localStorage.getItem('questions2'));
let currentPrice = 100;
let totalPrice = 0;
let variants = document.getElementsByClassName('question-variants');
let text = document.getElementById('questionText');
let total = document.getElementById('total');
let current = document.getElementById('current');
let questionArea = document.querySelector('.question');
let skip = document.getElementById('skip');
let final = document.getElementById('final');
let million = document.getElementById('million');
let correct;

skip.onclick = function() {
    showQuestion(quests);
    skip.style = 'display: none';
}

function showQuestion(arr) {
    final.style = 'display: none';
    total.innerHTML = `Total prize: ${totalPrice}`;
    current.innerHTML = `Prize of current round: ${currentPrice}`;
    skip.style = 'display: inline';
    questionArea.style = 'display: block';
    total.style = 'display: block';
    current.style = 'display: block';
    let randomElement = Math.floor(Math.random() * arr.length)
    correct = arr[randomElement]['correct'];

    text.innerHTML = `${arr[randomElement]['question']}`;

    for(let j = 0; j < variants.length; j++) {
        variants[j].addEventListener('click', checkResult);
        variants[j].id = '';
        variants[j].innerHTML = `${arr[randomElement]['content'][j]}`;
        if(correct === j) {
            variants[j].id = correct;
        }
    }
}

function checkResult() {
    if(this.id === String(correct)) {
        totalPrice += currentPrice;
        currentPrice *= 2;
        total.innerHTML = `Total prize:${totalPrice}`;
        current.innerHTML = `Prize of current round:${currentPrice}`;
        showQuestion(quests);
    } else {
        questionArea.style = 'display: none';
        skip.style = 'display: none';
        total.style = 'display: none';
        current.style = 'display: none';
        final.style = 'display: block';
        final.innerHTML = `Game over. Your prize is:${totalPrice}`;
        totalPrice = 0;
        currentPrice = 100;
    }   

    if(totalPrice > 1000000) {
        questionArea.style = 'display: none';
        skip.style = 'display: none';
        total.style = 'display: none';
        current.style = 'display: none';
        million.innerHTML = 'Congratulations! You won 1 000 000.';
    }
}