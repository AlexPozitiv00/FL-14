/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is not neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./js/addListeners.js":
/*!****************************!*\
  !*** ./js/addListeners.js ***!
  \****************************/
/*! namespace exports */
/*! export default [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__, __webpack_require__.r, __webpack_exports__, __webpack_require__.d, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => /* binding */ addEventListeners\n/* harmony export */ });\n/* harmony import */ var _js_onResetButtonClick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../js/onResetButtonClick */ \"./js/onResetButtonClick.js\");\n\nfunction addEventListeners(list, func) {\n  [].forEach.call(list, function (el) {\n    el.addEventListener('click', func);\n  });\n  var resetBtn = document.querySelector('#resetBtn');\n  resetBtn.addEventListener('click', _js_onResetButtonClick__WEBPACK_IMPORTED_MODULE_0__.default);\n}\n\n//# sourceURL=webpack://src/./js/addListeners.js?");

/***/ }),

/***/ "./js/battleResult.js":
/*!****************************!*\
  !*** ./js/battleResult.js ***!
  \****************************/
/*! namespace exports */
/*! export default [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_exports__, __webpack_require__.r, __webpack_require__.d, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => __WEBPACK_DEFAULT_EXPORT__\n/* harmony export */ });\nvar battleResult = function battleResult(playerMove, computerMove) {\n  switch (playerMove) {\n    case 'rock':\n      if (computerMove === 'rock') {\n        return '';\n      }\n\n      if (computerMove === 'paper') {\n        return 'Rock vs. Paper, You’ve LOST!';\n      }\n\n      if (computerMove === 'scissors') {\n        return 'Rock vs. Scissors, You’ve WON!';\n      }\n\n      break;\n\n    case 'paper':\n      if (computerMove === 'rock') {\n        return 'Paper vs. Rock. You’ve WON!';\n      }\n\n      if (computerMove === 'paper') {\n        return '';\n      }\n\n      if (computerMove === 'scissors') {\n        return 'Paper vs. Scissors, You’ve LOST!';\n      }\n\n    case 'scissors':\n      if (computerMove === 'rock') {\n        return 'Scissors vs. Rock. You’ve LOST!';\n      }\n\n      if (computerMove === 'paper') {\n        return 'Scissors vs. Paper, You’ve WON!';\n      }\n\n      if (computerMove === 'scissors') {\n        return '';\n      }\n\n    default:\n      break;\n  }\n};\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (battleResult);\n\n//# sourceURL=webpack://src/./js/battleResult.js?");

/***/ }),

/***/ "./js/clickHandler.js":
/*!****************************!*\
  !*** ./js/clickHandler.js ***!
  \****************************/
/*! namespace exports */
/*! export default [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__, __webpack_exports__, __webpack_require__.r, __webpack_require__.d, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => __WEBPACK_DEFAULT_EXPORT__\n/* harmony export */ });\n/* harmony import */ var _js_generateMoves__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../js/generateMoves */ \"./js/generateMoves.js\");\n/* harmony import */ var _js_battleResult__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../js/battleResult */ \"./js/battleResult.js\");\n/* harmony import */ var _js_displayResults__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../js/displayResults */ \"./js/displayResults.js\");\n/* harmony import */ var _js_countBattlesAmount__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../js/countBattlesAmount */ \"./js/countBattlesAmount.js\");\n\n\n\n\n\nvar onBtnClickHandler = function onBtnClickHandler(e) {\n  var resultsContainer = document.querySelector('.results-wrapper');\n\n  if ((0,_js_countBattlesAmount__WEBPACK_IMPORTED_MODULE_3__.default)() === 4) {\n    resultsContainer.innerHTML = '';\n  }\n\n  var variant = e.target.innerText.toLowerCase();\n  var res = (0,_js_battleResult__WEBPACK_IMPORTED_MODULE_1__.default)(variant, (0,_js_generateMoves__WEBPACK_IMPORTED_MODULE_0__.default)(variant));\n\n  if (res !== '') {\n    (0,_js_displayResults__WEBPACK_IMPORTED_MODULE_2__.default)(res);\n  }\n};\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (onBtnClickHandler);\n\n//# sourceURL=webpack://src/./js/clickHandler.js?");

/***/ }),

/***/ "./js/countBattlesAmount.js":
/*!**********************************!*\
  !*** ./js/countBattlesAmount.js ***!
  \**********************************/
/*! namespace exports */
/*! export default [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_exports__, __webpack_require__.r, __webpack_require__.d, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => __WEBPACK_DEFAULT_EXPORT__\n/* harmony export */ });\nvar countBattleAmount = function countBattleAmount() {\n  return document.querySelector('.results-wrapper').children.length;\n};\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (countBattleAmount);\n\n//# sourceURL=webpack://src/./js/countBattlesAmount.js?");

/***/ }),

/***/ "./js/defineWinner.js":
/*!****************************!*\
  !*** ./js/defineWinner.js ***!
  \****************************/
/*! namespace exports */
/*! export default [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_exports__, __webpack_require__.r, __webpack_require__.d, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => __WEBPACK_DEFAULT_EXPORT__\n/* harmony export */ });\nvar defineWinner = function defineWinner() {\n  var childrens = Array.from(document.querySelector('.results-wrapper').children);\n  var results = childrens.map(function (c) {\n    return c.innerText;\n  });\n  var countWins = results.filter(function (str) {\n    return str.toLowerCase().includes('won');\n  }).length;\n  if (countWins >= 2) return 'you';\n  return 'computer';\n};\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (defineWinner);\n\n//# sourceURL=webpack://src/./js/defineWinner.js?");

/***/ }),

/***/ "./js/displayResults.js":
/*!******************************!*\
  !*** ./js/displayResults.js ***!
  \******************************/
/*! namespace exports */
/*! export default [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__, __webpack_exports__, __webpack_require__.r, __webpack_require__.d, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => __WEBPACK_DEFAULT_EXPORT__\n/* harmony export */ });\n/* harmony import */ var _js_countBattlesAmount__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../js/countBattlesAmount */ \"./js/countBattlesAmount.js\");\n/* harmony import */ var _js_defineWinner__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../js/defineWinner */ \"./js/defineWinner.js\");\n\n\n\nvar displayResults = function displayResults(text) {\n  var resultsContainer = document.querySelector('.results-wrapper');\n  var div = document.createElement('div');\n  div.setAttribute('class', 'result-field');\n\n  if (text != '') {\n    div.innerText = text;\n    resultsContainer.appendChild(div);\n  }\n\n  if ((0,_js_countBattlesAmount__WEBPACK_IMPORTED_MODULE_0__.default)() === 3) {\n    var winner = (0,_js_defineWinner__WEBPACK_IMPORTED_MODULE_1__.default)();\n    div = document.createElement('div');\n    div.setAttribute('class', 'result-field');\n\n    if (winner === 'you') {\n      div.innerText = 'You won this battle!';\n    } else {\n      div.innerText = 'You lost this battle!';\n    }\n\n    resultsContainer.appendChild(div);\n    return;\n  }\n};\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (displayResults);\n\n//# sourceURL=webpack://src/./js/displayResults.js?");

/***/ }),

/***/ "./js/generateMoves.js":
/*!*****************************!*\
  !*** ./js/generateMoves.js ***!
  \*****************************/
/*! namespace exports */
/*! export default [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_exports__, __webpack_require__.r, __webpack_require__.d, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => __WEBPACK_DEFAULT_EXPORT__\n/* harmony export */ });\nvar generateMove = function generateMove(variant) {\n  var possibleMoves = ['rock', 'paper', 'scissors'];\n  var randomNum = Math.floor(Math.random() * 2);\n\n  while (possibleMoves[randomNum] === variant) {\n    randomNum = Math.floor(Math.random() * 2);\n  }\n\n  return possibleMoves[randomNum];\n};\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (generateMove);\n\n//# sourceURL=webpack://src/./js/generateMoves.js?");

/***/ }),

/***/ "./js/index.js":
/*!*********************!*\
  !*** ./js/index.js ***!
  \*********************/
/*! namespace exports */
/*! exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__, __webpack_require__.r, __webpack_exports__, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _sass_styles_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../sass/styles.scss */ \"./sass/styles.scss\");\n/* harmony import */ var _js_addListeners__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../js/addListeners */ \"./js/addListeners.js\");\n/* harmony import */ var _js_clickHandler__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../js/clickHandler */ \"./js/clickHandler.js\");\n\n\n\nvar buttons = document.querySelectorAll('.btn-choice');\n(0,_js_addListeners__WEBPACK_IMPORTED_MODULE_1__.default)(buttons, _js_clickHandler__WEBPACK_IMPORTED_MODULE_2__.default);\n\n//# sourceURL=webpack://src/./js/index.js?");

/***/ }),

/***/ "./js/onResetButtonClick.js":
/*!**********************************!*\
  !*** ./js/onResetButtonClick.js ***!
  \**********************************/
/*! namespace exports */
/*! export default [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_exports__, __webpack_require__.r, __webpack_require__.d, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => __WEBPACK_DEFAULT_EXPORT__\n/* harmony export */ });\nvar onResetButtonClick = function onResetButtonClick() {\n  var resultsContainer = document.querySelector('.results-wrapper');\n  resultsContainer.innerHTML = '';\n};\n\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (onResetButtonClick);\n\n//# sourceURL=webpack://src/./js/onResetButtonClick.js?");

/***/ }),

/***/ "./sass/styles.scss":
/*!**************************!*\
  !*** ./sass/styles.scss ***!
  \**************************/
/*! namespace exports */
/*! exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__.r, __webpack_exports__, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack://src/./sass/styles.scss?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop)
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	// startup
/******/ 	// Load entry module
/******/ 	__webpack_require__("./js/index.js");
/******/ 	// This entry module used 'exports' so it can't be inlined
/******/ })()
;