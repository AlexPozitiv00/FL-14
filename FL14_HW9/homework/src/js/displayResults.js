import countBattleAmount from '../js/countBattlesAmount';
import defineWinner from '../js/defineWinner';

const displayResults = (text) => {
  let resultsContainer = document.querySelector('.results-wrapper');
  let div = document.createElement('div');
  div.setAttribute('class', 'result-field');
  if (text != '') {
    div.innerText = text;
    resultsContainer.appendChild(div);
  }
  if (countBattleAmount() === 3) {
    let winner = defineWinner();
    div = document.createElement('div');
    div.setAttribute('class', 'result-field');
    if (winner === 'you') {
      div.innerText = 'You won this battle!';
    } else {
      div.innerText = 'You lost this battle!';
    }
    resultsContainer.appendChild(div);
    return;
  }
};
export default displayResults;
