import onResetButtonClick from '../js/onResetButtonClick';

export default function addEventListeners(list, func) {
  [].forEach.call(list, function (el) {
    el.addEventListener('click', func);
  });
  const resetBtn = document.querySelector('#resetBtn');
  resetBtn.addEventListener('click', onResetButtonClick);
}
