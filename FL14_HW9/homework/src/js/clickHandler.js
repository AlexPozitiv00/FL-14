import generateMove from '../js/generateMoves';
import battleResult from '../js/battleResult';
import displayResults from '../js/displayResults';
import countBattleAmount from '../js/countBattlesAmount';

const onBtnClickHandler = (e) => {
  let resultsContainer = document.querySelector('.results-wrapper');
  if (countBattleAmount() === 4) {
    resultsContainer.innerHTML = '';
  }
  let variant = e.target.innerText.toLowerCase();
  let res = battleResult(variant, generateMove(variant));
  if (res !== '') {
    displayResults(res);
  }
};
export default onBtnClickHandler;
