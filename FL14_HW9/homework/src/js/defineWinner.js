const defineWinner = () => {
  let childrens = Array.from(
    document.querySelector('.results-wrapper').children
  );
  let results = childrens.map((c) => c.innerText);
  let countWins = results.filter((str) => str.toLowerCase().includes('won'))
    .length;
  if (countWins >= 2) return 'you';
  return 'computer';
};

export default defineWinner;
