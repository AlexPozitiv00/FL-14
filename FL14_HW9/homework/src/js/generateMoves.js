const generateMove = (variant) => {
  const possibleMoves = ['rock', 'paper', 'scissors'];
  let randomNum = Math.floor(Math.random() * 2);
  while (possibleMoves[randomNum] === variant) {
    randomNum = Math.floor(Math.random() * 2);
  }
  return possibleMoves[randomNum];
};
export default generateMove;
