const onResetButtonClick = () => {
  let resultsContainer = document.querySelector('.results-wrapper');
  resultsContainer.innerHTML = '';
};

export default onResetButtonClick;
