import '../sass/styles.scss';
import addEventListeners from '../js/addListeners';
import onBtnClickHandler from '../js/clickHandler';

const buttons = document.querySelectorAll('.btn-choice');
addEventListeners(buttons, onBtnClickHandler);
