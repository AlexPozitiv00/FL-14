const battleResult = (playerMove, computerMove) => {
  switch (playerMove) {
    case 'rock':
      if (computerMove === 'rock') {
        return '';
      }
      if (computerMove === 'paper') {
        return 'Rock vs. Paper, You’ve LOST!';
      }
      if (computerMove === 'scissors') {
        return 'Rock vs. Scissors, You’ve WON!';
      }
      break;
    case 'paper':
      if (computerMove === 'rock') {
        return 'Paper vs. Rock. You’ve WON!';
      }
      if (computerMove === 'paper') {
        return '';
      }
      if (computerMove === 'scissors') {
        return 'Paper vs. Scissors, You’ve LOST!';
      }
    case 'scissors':
      if (computerMove === 'rock') {
        return 'Scissors vs. Rock. You’ve LOST!';
      }
      if (computerMove === 'paper') {
        return 'Scissors vs. Paper, You’ve WON!';
      }
      if (computerMove === 'scissors') {
        return '';
      }
    default:
      break;
  }
};
export default battleResult;
