// Your code goes here
class Deck {
    constructor() {
        let suit = ['Hearts', 'Diamonds', 'Clubs', 'Spades'];
        this.cards = [];
        let totalQuantity = 13;
        suit.forEach((item) => {
            for(let i = 0; i<= totalQuantity; i++) {
                let card = new Card(item, i);
                this.cards.push(card);
            }
        })
    }

    get count() {
        this._count = this.cards.length;
        return this._count;
    }

    shuffle() {
        let half = 0.5;
        this.cards.sort(() => Math.random() - half)
    }

    draw(n) {
        let deletedElements = this.cards.slice(-n);
        this.cards = this.cards.slice(0, -n);
        return deletedElements;
    }
}


class Card {
    constructor(suit, rank) {
        this.suit = suit[0].toUpperCase() + suit.slice(1);
        this.rank = rank;
    }

    get isFasedCard() {
        let typeOfCard;
        if(this.rank > 10 || this.rank === 1) {
            typeOfCard = true;
        } else {
            typeOfCard = false;
        }

        this._isFasedCard = typeOfCard;
        return this._isFasedCard;
    }

    toString() {
        let cards = [
            'Ace', 'Two', 'Three', 'Four',
            'Five', 'Six', 'Seven', 'Eight',
            'Nine', 'Ten', 'Jack', 'Queen', 'King'
        ];
        return `${cards[this.rank - 1]} of ${this.suit}`
    }

    static compare(cardOne, cardTwo) {
        return cardTwo.rank - cardOne.rank;
    }
}


class Player {
    constructor(name, deck) {
        this.name = name;
        this.deck = deck;
        this._wins = 0;
    }

    get wins() {
        return this._wins;
    }

    static play(playerOne, playerTwo) {
        playerOne.deck.shuffle();
        playerTwo.deck.shuffle();
        while(playerTwo.deck.count > 0 || playerOne.deck.count > 0) {
            let cardOne = playerOne.deck.draw(1)[0];
            let cardTwo = playerTwo.deck.draw(1)[0];
            let result = Card.compare(cardOne, cardTwo);
            if(result > 0) {
                playerTwo._wins++;
            } else if(result < 0) {
                playerOne._wins++;
            } else {
                continue;
            }
        }

        let message = 'It`s a draw!';
        if(playerOne.wins > playerTwo.wins) {
            message = `${playerOne.name} wins ${playerOne.wins} to ${playerTwo.wins}`;
            return message;
        } else if(playerOne.wins < playerTwo.wins) {
            message = `${playerTwo.name} wins ${playerTwo.wins} to ${playerOne.wins}`;
            return message;
        } else {
            return message;
        }
    }
}


let player1 = new Player('Inna', new Deck());
let player2 = new Player('Sasha', new Deck());

alert(Player.play(player1, player2));


//---------Task 2-----------
class Employee {
    constructor(id, firstName, lastName, birthday, salary, position, department) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this._age = this.age;
        this._fullName = this.fullName;
        Employee.EMPLOYEES.push(this);
    }

    static get EMPLOYEES() {
        if(this.list === undefined) {
            this.list = [];
        }
        return this.list;
    }

    quit() {
        Employee.list.forEach(el => {
            if(this.id === el.id) {
                const indexOfThisElem = Employee.list.indexOf(this);
                return Employee.list.splice(indexOfThisElem, 1);
            }
        })
    }

    get age() {
        const birthdayDate = new Date(this.birthday);
        const today = new Date();
        const month = today.getMonth() - birthdayDate.getMonth();
        let age = today.getFullYear() - birthdayDate.getFullYear();
        if(month < 0 || month === 0 && today.getDate() < birthdayDate.getDate()) {
            age--;
            this._age = age;
        } else {
            this._age = age;
        }
        
        return this._age;
    }
    
    get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }

    changeDepartment(newDepartment) {
        this.department = newDepartment;
    }

    changePosition(newPosition) {
        this.position = newPosition;
    }

    channgeSalary(newSalary) {
        this.salary = newSalary;
    }

    retire() {
        console.log('It was such a pleasure to work with you!');
        this.quit();
    }

    getFired() {
        console.log('Not a big deal!');
        this.quit()
    }

    getPromoted(benefits) {
        if(this.department !== undefined || this.salary !== undefined || this.position !== undefined) {
            console.log('Yoohooo!');
            let department = benefits.department ? benefits.department : this.department;
            let salary = benefits.salary ? benefits.salary : this.salary;
            let position = benefits.position ? benefits.position : this.position;
            Employee.list.find(elem => elem === this).department = department;
            Employee.list.find(elem => elem === this).salary = salary;
            Employee.list.find(elem => elem === this).position = position;
        }
    }
}


class Manager extends Employee {
    constructor(...args) {
        super(...args)
        this._position = 'manager';
    }

    get managerEmployees() {
        return Employee.list.filter(item => item.position !== 'manager' && item.department === this.department);
    }
}


class BlueCollarWorker extends Employee {

}


class HRManager extends Employee {
    constructor(...args) {
        super(...args)
        this._department = 'hr';
    }
}


class SalesManager extends Manager {
    constructor(...args) {
        super(...args)
        this._department = 'sales';
    }
}
