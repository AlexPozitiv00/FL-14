// Your code goes here
//Task 1
let maxElement = array => {
    return Math.max(...array);
}
const array1 = [2, 13, 56, 76];
console.log('---Task 1---');
console.log(maxElement(array1));

//Task 2
let copyArray = array => {
    let copiedArray = [...array];
    return copiedArray;
}
const array2 = [1,2,3];
const copiedArray = copyArray(array2);
console.log('---Task 2---');
console.log(array2);
console.log(copiedArray);
console.log(array2 === copiedArray);

//Task 3
function addUniqueId(obj) {
    let return_object = {name: obj.name};
    return_object.id = Symbol(obj.name);
    return return_object;
}
let obj = {
    name: 'Alex',
    surname: 'Pozitiv'
}
let obj2 = addUniqueId({id: 123});
console.log('---Task 3---');
console.log(obj);
console.log(obj2);

//Task 4
let oldObj = {
    name: 'SomeOne',
    details: {
        id: 1,
        age: 11,
        university: 'UNI'
    }
};
function regroupObject(oldObj) {
    let newObj = {};
   let {
       name: newName,
       details: {
           id,
           age, 
           university
       }
   } = oldObj;

   newObj.university = university;
   newObj.user = {
        age: age,
        firstName: newName,
        id: id
   }
   return newObj;
}
console.log('---Task 4---');
console.log(regroupObject(oldObj));

//Task 5
let arr = [1,1,1,3,4,53,3,4,5,53];

let findUniqueElements = arr => {
    let unique = [...new Set(arr)]; 
    return unique;
}
console.log('---Task 5---');
console.log(findUniqueElements(arr));

//Task 6 
const phoneNumber = '0123456789';

function hideNumber(str) {
    let cutStr = str.slice(6);
    let newStr = cutStr.padStart(10, "*");
    return newStr;
}
console.log('---Task 6---');
console.log(hideNumber(phoneNumber));

//Task 7
function add(a, b) {
    if(arguments.length < 2) {
        throw new Error('Missing property');
    }
    return a + b;
}
console.log('---Task 7---')
console.log(add(1, 2));
//console.log(add(1));

//Task 8

let requestURL = 'https://jsonplaceholder.typicode.com/users'
//Promise
fetch(requestURL)
.then(response => {
    console.log('---Task 8---');
    console.log('Promise');
    return response.json();
}).then(response => { 
    console.log(response.sort((a,b) => { 
    return a.name.localeCompare(b.name)
}))
})


//Task 9

//Async Await
async function sendRequest() {
    let response = await fetch(requestURL);
    let userArr = await response.json();
    console.log('---Task 9---');
    console.log('Async await');
    console.log(userArr.sort((a,b) => { 
        return a.name.localeCompare(b.name)
    }));
 }
 sendRequest();
 


  